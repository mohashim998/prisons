@extends('layouts.app')
           @include("layouts.includes.navbar")
@section('content')

<div id="wrapper">

  @include("layouts.includes.sidebar")

  <div id="navbar-wrapper">
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a href="#" class="navbar-brand" id="sidebar-toggle"><i class="fa fa-bars"></i></a>
        </div>
      </div>
    </nav>
  </div>

  <section id="content-wrapper">
    

    <div class="stats-boxes">
        <div class="row">
            <div class="col-md-3">
                <div class="state-box">
                      <div class="details">
                        <h2>قوات  الامن </h2>
                        <h3>  36 </h3>
                    </div>
                    <i class="fas fa-users"></i>
                </div>
            </div>
            <div class="col-md-3">
                <div class="state-box">
                      <div class="details">
                        <h2>  التقارير </h2>
                        <h3>  14 </h3>
                    </div>
                    <i class="fas fa-file"></i>
                </div>
            </div>
            <div class="col-md-3">
                <div class="state-box">
                      <div class="details">
                        <h2>الرسائل   </h2>
                        <h3>  39 </h3>
                    </div>
                     <i class="fas fa-comments"></i>

                </div>
            </div>
            <div class="col-md-3">
                <div class="state-box">
                    <div class="details">
                        <h2>  المستندات </h2>
                        <h3>  23 </h3>
                    </div>
                    <i class="fas fa-file-alt"></i>
                </div>
            </div>
        </div>
    </div>




  </section>

</div>




@section('scripts')

<script>
    const $button  = document.querySelector('#sidebar-toggle');
const $wrapper = document.querySelector('#wrapper');

$button.addEventListener('click', (e) => {
  e.preventDefault();
  $wrapper.classList.toggle('toggled');
});
</script>

@endsection


@endsection
