<?php

namespace App\Http\Controllers;

use App\Soldier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SoldierController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $soldiers=Soldier::paginate(3);
        return view('dashboard.soldires' ,['soldiers' =>$soldiers]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Form.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validData= $request->validate([
            'full_name' => 'string|required',
            'police_number' => 'string|required',
            'personal_phone' => 'string|required',
            'national_id' => 'string|required',
            'rank' => 'string|required',
            'qualification' => 'string|required',
            'address' => 'string|required',
            'governorate' => 'string|required',
            'destination' => 'string|required',
            'given_job' => 'string|required',
            'leave_date' => 'string|required',
            'join_date' => 'string|required',
            'birthdate' => 'string|required',
            'avatar' => 'sometimes|image|mimes:jpg,png,jpeg',
            'criminal_dispatch_date' => 'required',
            'criminal_dispatch_number' => 'required' ,
            'political_dispatch_date' => 'required' ,
            'political_dispatch_number' => 'required',
            'criminal_dispatch_result' => 'required' ,
            'political_dispatch_result' => 'required' ,
            'military_trials' => 'required' ,
            'disciplinary_violations'  => 'required',
            'medical_decisions' => 'required'
        ]);

        if ($request->hasFile('avatar')){
            $image = $request->avatar;
            $imageName = time() . "_" . rand(1,1000) . "." . $image->extension();
            $image->move(public_path('images/soldiers'), $imageName);

              Soldier::create([
                  'full_name' => $validData['full_name'],
                  'police_number' => $validData['police_number'],
                  'personal_phone' => $validData['personal_phone'],
                  'national_id' => $validData['national_id'],
                  'rank' => $validData['rank'],
                  'qualification' => $validData['qualification'],
                  'address' => $validData['address'],
                  'governorate' => $validData['governorate'],
                  'destination' => $validData['destination'],
                  'given_job' => $validData['given_job'],
                  'leave_date' => $validData['leave_date'],
                  'join_date' => $validData['join_date'],
                  'birthdate' => $validData['birthdate'],
                  'avatar' => $imageName,
                  'criminal_dispatch_date' => $validData['criminal_dispatch_date'],
                  'criminal_dispatch_number' => $validData['criminal_dispatch_number'] ,
                  'political_dispatch_date' => $validData['political_dispatch_date'] ,
                  'political_dispatch_number' => $validData['political_dispatch_number'],
                  'criminal_dispatch_result' => $validData['criminal_dispatch_result'] ,
                  'political_dispatch_result' => $validData['political_dispatch_result'] ,
                  'military_trials' => $validData['military_trials'] ,
                  'disciplinary_violations'  => $validData['disciplinary_violations'],
                  'medical_decisions' => $validData['medical_decisions']
            ]);
        }else{
            $soldier =  Soldier::create([
                'full_name' => $validData['full_name'],
                'police_number' => $validData['police_number'],
                'personal_phone' => $validData['personal_phone'],
                'national_id' => $validData['national_id'],
                'rank' => $validData['rank'],
                'qualification' => $validData['qualification'],
                'address' => $validData['address'],
                'governorate' => $validData['governorate'],
                'destination' => $validData['destination'],
                'given_job' => $validData['given_job'],
                'leave_date' => $validData['leave_date'],
                'join_date' => $validData['join_date'],
                'birthdate' => $validData['birthdate'],
                'criminal_dispatch_date' => $validData['criminal_dispatch_date'],
                'criminal_dispatch_number' => $validData['criminal_dispatch_number'] ,
                'political_dispatch_date' => $validData['political_dispatch_date'] ,
                'political_dispatch_number' => $validData['political_dispatch_number'],
                'criminal_dispatch_result' => $validData['criminal_dispatch_result'] ,
                'political_dispatch_result' => $validData['political_dispatch_result'] ,
                'military_trials' => $validData['military_trials'] ,
                'disciplinary_violations'  => $validData['disciplinary_violations'],
                'medical_decisions' => $validData['medical_decisions']
            ]);
        }



        return redirect()->route('soldiers.index')->with('success' , 'تم اضافه البيانات بنجاح');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Soldier  $soldier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Soldier  $soldier
     * @return \Illuminate\Http\Response
     */
    public function edit(Soldier $soldier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Soldier  $soldier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Soldier $soldier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Soldier  $soldier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delsoldier = Soldier::findOrFail($id);
        File::delete('images/soldiers/' . $delsoldier->avatar);

        $delsoldier->delete();

        return redirect()->route('soldiers.index')->with('success' , 'تم حذف البيانات بنجاح');
    }
}
