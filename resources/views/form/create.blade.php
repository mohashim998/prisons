

@extends('layouts.app')
           @include("layouts.includes.navbar")
@section('content')

<div id="wrapper">

  @include("layouts.includes.sidebar")

  <div id="navbar-wrapper">
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a href="#" class="navbar-brand" id="sidebar-toggle"><i class="fa fa-bars"></i></a>
        </div>
      </div>
    </nav>
  </div>

  <section id="content-wrapper">
         <div class="container">
        <div class=" justify-content-center">
            <div class="card ">
                <div class="card-header text-center">
                    <h3>ادخال البيانات (العساكر و الظباط)  </h3>
                </div>

                <div class="card-body text-right">


                    @if ($errors->any())
                    <div class="alert alert-danger m-1">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <form class="my-form row" action="{{route('soldiers.store')}}" method="post" enctype="multipart/form-data">
                        @csrf


                        <div class="col-md-6">
                            <div class="form-group">
                                <label> الرقم القومي </label>
                                <input class="form-control" type="text" name="national_id" placeholder="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>الأسم بالكامل</label>
                                <input class="form-control" type="text" name="full_name" placeholder="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label> الصوره الشخصيه </label>
                                <input class="form-control" type="file" name="avatar" placeholder="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>  الدرجه </label>
                                <input class="form-control" type="text" name="rank" placeholder="">
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label>  رقم  الشرطه </label>
                                <input class="form-control" type="text" name="police_number" placeholder="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label> رقم الهاتف</label>
                                <input class="form-control" type="text" name="personal_phone" placeholder="">
                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="form-group">
                                <label>المؤهل </label>
                                <select class="form-control" name="qualification">
                                    <option readonly> اختر المؤهل</option>
                                    <option value="3">مؤهل عالي</option>
                                    <option value="2">مؤهل فوق المتوسط</option>
                                    <option value="1">مؤهل المتوسط</option>
                                </select>
                            </div>
                        </div>




                        <div class="col-md-6">
                            <div class="form-group">
                                <label>   تاريخ  الميلاد </label>
                                <input class="form-control" type="date" name="birthdate" placeholder="">
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label>   تاريخ انتهاء التجنيد </label>
                                <input class="form-control" type="date" name="leave_date" placeholder="">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>   تاريخ  التجنيد </label>
                                <input class="form-control" type="date" name="join_date" placeholder="">
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label>     العمل المسند اليه </label>
                                <input class="form-control" type="text" name="given_job" placeholder="">
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label>     الجهه </label>
                                <input class="form-control" type="text" name="destination" placeholder="">
                            </div>
                        </div>




                        <div class="col-md-6">
                            <div class="form-group">
                                <label>   العنوان  </label>
                                <input type="text" class="form-control" name="address">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>   المحافظه  </label>
                                <input type="text" class="form-control" name="governorate">
                            </div>
                        </div>

                        <hr/>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>   رقم  الارسال الجنائى  </label>
                                <input type="text" class="form-control" name="criminal_dispatch_number">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>   تاريخ الارسال الجنائي  </label>
                                <input type="date" class="form-control" name="criminal_dispatch_date">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>   رقم  الارسال السياسي  </label>
                                <input type="text" class="form-control" name="political_dispatch_number">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>   تاريخ الارسال السياسي  </label>
                                <input type="date" class="form-control" name="political_dispatch_date">
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label>   نتيجه الارسال السياسي  </label>
                                <input type="text" class="form-control" name="political_dispatch_result">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>   نتيجه الارسال الجنائي  </label>
                                <input type="text" class="form-control" name="criminal_dispatch_result">
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <label>   المحكمات العسكرية  </label>
                                <textarea cols="5" rows="5" class="form-control" name="military_trials"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>    المخالفات االنضباطية  </label>
                                <textarea cols="5" rows="5" class="form-control" name="disciplinary_violations"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>    القرارات الطبية  </label>
                                <textarea cols="5" rows="5" class="form-control" name="medical_decisions"></textarea>
                            </div>
                        </div>








                        <div class="form-group text-center m-auto">
                            <input class="btn btn-outline-primary" type="submit" value="ادخال البيانات">
                        </div>







                    </form>

                </div>
            </div>
        </div>
    </div>
  </section>

</div>




@section('scripts')

<script>
    const $button  = document.querySelector('#sidebar-toggle');
const $wrapper = document.querySelector('#wrapper');

$button.addEventListener('click', (e) => {
  e.preventDefault();
  $wrapper.classList.toggle('toggled');
});
</script>

@endsection


@endsection








