<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoldiersTable extends Migration
{

    public function up()
    {
        Schema::create('soldiers', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('address');
            $table->string('rank');
            $table->string('governorate');
            $table->string('police_number');
            $table->string('personal_phone');
            $table->date('join_date');
            $table->date('leave_date');
            $table->date('birthdate');
            $table->enum('qualification' , ['1','2','3']);
            $table->string('destination');
            $table->string('given_job');
            $table->string('criminal_dispatch_date');
            $table->string('criminal_dispatch_number');
            $table->string('political_dispatch_date');
            $table->string('political_dispatch_number');
            $table->string('criminal_dispatch_result');
            $table->string('political_dispatch_result');
            $table->string('military_trials');
            $table->string('disciplinary_violations');
            $table->string('medical_decisions');
            $table->string('avatar')->nullable();
            $table->string('insurance_number')->nullable();
            $table->string('national_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soldiers');
    }
}
