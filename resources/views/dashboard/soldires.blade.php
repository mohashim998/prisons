

@extends('layouts.app')
           @include("layouts.includes.navbar")
@section('content')

<div id="wrapper">

  @include("layouts.includes.sidebar")

  <div id="navbar-wrapper">
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a href="#" class="navbar-brand" id="sidebar-toggle"><i class="fa fa-bars"></i></a>
        </div>
      </div>
    </nav>
  </div>

  <section id="content-wrapper">
     <div class="row">
        <div class="col-lg-12">
              <div class=" justify-content-center">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    <h3>المديريه العامه للسجون (العساكر و الظباط)  </h3>

                  <a href="{{route('soldiers.create')}}" class="btn btn-primary">اضافه عسكري / ظابط</a>
                </div>

                <div class="card-body p-0">
                    @if (session('success'))
                        <div class="alert alert-success text-right m-1">
                            {{ session('success') }}
                        </div>
                    @endif

                        <table dir="rtl" class="table table-bordered text-right mb-0">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">الصوره</th>
                                <th scope="col">الاسم</th>
                                <th scope="col">الرتبه</th>
                                <th scope="col">الرقم القومي </th>
                                <th scope="col"> رقم الهاتف </th>
                                <th scope="col">  التحكم </th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($soldiers))
                                @foreach($soldiers as $soldier)
                                    <tr>
                                        <th scope="row">{{$soldier->id}}</th>
                                        <td>
                                            @if(!empty($soldier->avatar))
                                            <img class="rounded-circle" height="50" width="50" src="{{asset('images/soldiers/' . $soldier->avatar)}}">
                                            @else
                                                <img class="rounded-circle" height="50" width="50" src="{{asset('images/soldiers/avatar.png')}}">
                                            @endif
                                        </td>
                                        <td>{{$soldier->full_name}}</td>
                                        <td>{{$soldier->rank}}</td>
                                        <td>{{$soldier->national_id}}</td>
                                        <td>{{$soldier->personal_phone}}</td>
                                        <td>

                                            <form method="POST" action="{{ route('soldiers.destroy',$soldier->id) }}">
                                                {{ csrf_field() }}
                                                {{ method_field('delete') }}
                                                <button class="btn btn-sm btn-outline-danger" type="submit">
                                                    <i class="far fa-trash-alt"></i>
                                                    حذف البيانات
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                            <div class="alert alert-warning m-1 text-right">
                                There is no data to show
                            </div>
                            @endif

                            </tbody>
                            {{$soldiers->links()}}
                        </table>


                </div>
            </div>
        </div>
        </div>
      </div>
  </section>

</div>




@section('scripts')

<script>
    const $button  = document.querySelector('#sidebar-toggle');
const $wrapper = document.querySelector('#wrapper');

$button.addEventListener('click', (e) => {
  e.preventDefault();
  $wrapper.classList.toggle('toggled');
});
</script>

@endsection


@endsection




