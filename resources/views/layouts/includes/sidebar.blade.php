
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <h2>لوحه التحكم</h2>
    </div>
    <ul class="sidebar-nav">
      <li class="active">
        <a href="{{route('home')}}"><i class="fa fa-home"></i>الرئيسيه</a>
      </li>
      <li>
        <a href="{{route('soldiers.index')}}"><i class="fa fa-users"></i>القوات</a>
      </li>
      <li>
        <a href="#"><i class="fa fa-file-alt"></i>التقارير</a>
      </li>
      <li>
        <a href="#"><i class="fa fa-comments"></i>الرسائل</a>
      </li>
      <li>
        <a href="#"><i class="fa fa-file"></i>المستندات</a>
      </li>
      <li>
        <a href="#"><i class="fa fa-cogs"></i>الاعدادت</a>
      </li>
    </ul>
  </aside>

<script>
  document.addEventListener('DOMContentLoaded', () => {
    const navbar = document.querySelector('.navbar');
    const sidebar = document.querySelector('#sidebar-wrapper');
    const navbarHeight = navbar.innerHeight;
    window.onscroll = () => {
      stickySidebar(window.scrollY, navbar.offsetHeight)
    }

    function stickySidebar(YAxis, navHeight) {
      YAxis >= navHeight + 4 ?
        sidebar.classList.add('sticky')
        : sidebar.classList.remove('sticky')
    }
  })
  

  
</script>