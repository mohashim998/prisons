<?php $navbar = "no" ?>
@extends('layouts.app')
@section('styles')
<style>


.login-page{
    width: 100%;
    height: 100%;
    margin: 0;
    padding:0;
}

.card{
    margin: 60px auto;
    width: 350px
}


.card-header {
    padding: 0.75rem 1.25rem;
    margin-bottom: 0;
    background-color:#02000F !important;
    border-bottom: 1px solid rgba(0, 0, 0, 0.125);
    color: #fff;
}

.login-form .form-check-label{
margin-right: 20px;
}


.login-form .btn-primary {
    color: #fff;
    display: block;
    background-color: #3490dc;
    border-color: #3490dc;
    width: 100%;
}
</style>
@endsection
@section('content')


<div class="login-page">

    <div class="row ">

        <div class="col-md-6">
            <div class="card">
                <div class="card-header text-center">تسجيل الدخول</div>

                <div class="card-body">
                    <form class="login-form" method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email" class="col-form-label text-md-right">البريد الالكتروني</label>

                            <div class="">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="password" class=" col-form-label text-md-right">كلمه المرور</label>

                            <div class="">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        تذكرني
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    تسجيل
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

       <div class="col-md-6">
            <div class="img-wrapper">
                <img height="600" class="img-fluid" src="{{asset('images/site/logo1.jpg')}}" alt="">
            </div>
        </div>


    </div>
</div>

@endsection
