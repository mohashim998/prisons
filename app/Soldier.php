<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soldier extends Model
{
    protected $fillable = [
       'full_name' , 'address' , 'rank' , 'governorate' ,
        'police_number' , 'personal_phone' , 'join_date' , 'leave_date',
        'birthdate' , 'qualification' , 'destination' , 'given_job' ,
        'criminal_dispatch_date',
        'criminal_dispatch_number' ,
        'political_dispatch_date' ,
        'political_dispatch_number',
        'criminal_dispatch_result' ,
        'political_dispatch_result' ,
        'military_trials' ,
        'disciplinary_violations' ,
        'medical_decisions' ,
        'avatar' ,
        'insurance_number' ,
        'national_id'
    ];


}
